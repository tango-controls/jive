[![Documentation Status](https://readthedocs.org/projects/jive/badge/?version=latest)](http://jive.readthedocs.io/en/latest/?badge=latest)

[![Download](https://api.bintray.com/packages/tango-controls/maven/Jive/images/download.svg) ](https://bintray.com/tango-controls/maven/Jive/_latestVersion)

# jive
Jive is a standalone JAVA application designed to browse and edit the static TANGO database.

# Change Log

* V 7.45
    * Added multiple nodes selection from Classes/Devices node
    * Multiple device selection improvement (exported device filter, save attribute list)

* V 7.44
* V 7.43
    * Updated maven plugin dependencies

* V 7.42
    * Added keybaord shortcut in property panel and tree contextual menu (F2,DELETE,CTRL+C)

* V 7.41
    * Fix issue #72, Add warning message on delete admin device

* V 7.40
    * Update: Added Tango version view in admin panel (Test Device)

* V 7.39
    * Update: Added "Save To File" button in MultipePropertySelection panel

* V 7.37
    * Dev Update: New MainPanel constructor with tango host

* V 7.36
    * Update: Allow to check multiple resource files at once

* V 7.34
    * Fix: Fix device attribute node not expanded when coming from class
    * Fix: Fix a bug in PASTE from device node

* V 7.33
    * Update: Added execution time in MultipleTestDevice panel

* V 7.32/7.31
    * Dev Update: Moved saveServerData() to DbFileWriter class as public scope

* V 7.30
    * Fix: Fix command/attribute interface update in MultipeTestDevice panel
    * Update: Added confirmation dialog before command execution

* V 7.29
    * Fix: Fix tango host title in TestDevice panel

* V 7.27
    * Fix: Collection not displayed when change TangoHost.

* V 7.26
    * Fix: Wrong escape sequence when resource value contains '\\'
    * Fix: Wrong parsing when resource value contains an item equal to a special char [',' , '/' , '\\' , ':' , '-\>' ]
    
