# Command line

## Dependencies (CLASSPATH)

```
ASTOR=$TANGO_JAVA_APPLIS/Astor.jar
JSSHTERMINAL=$TANGO_JAVA_APPLIS/JSSHTerminal.jar
LOGVIEWER=$TANGO_JAVA_APPLIS/LogViewer.jar
LOG4J=$TANGO_JAVA_LIBS/log4j.jar
TANGO=$TANGO_JAVA_LIBS/JTango.jar
TANGOATK=$TANGO_JAVA_LIBS/ATKCore.jar:$TANGO_JAVA_LIBS/ATKWidget.jar
ATKPANEL=$TANGO_JAVA_APPLIS/atkpanel.jar
JIVE=$TANGO_JAVA_APPLIS/Jive.jar

CLASSPATH=$TANGO:$TACO:$TANGOATK:$ATKPANEL:$JIVE:$LOGVIEWER:$LOG4J:$ASTOR
```

Note: Astor.jar is needed for "Polling thread manager" and "Device dependencies" features.

## Main Jive application

```console
java -DTANGO_HOST=host:port jive3.MainPanel
```

```console
Usage: jive [-r] [-s server] [-d device] [-fxx filter] [-p panel]
 -r        Read only mode (No write access to database allowed)
 -s server Open jive and show specified server node (server=ServerName/instance)
 -d device Open jive and show specified device node (device=domain/family/member)
 -fs filter Default server filter
 -fd filter Default device filter
 -fc filter Default class filter
 -fa filter Default alias filter
 -faa filter Default attribute alias filter
 -fp filter Default property filter
 -p panelmask (1=Collection 2=Server 4=Device 8=Class 16=DevAlias 32=AttAlias 64=FreeProperty)
```

## Command line tools

_Make a diff between a property file and the DB_
```console
>java -DTANGO_HOST=host:port jive3.DbDiff test.res
test.res : matches
```

_Output all properties of a Tango server_
```console
>java -DTANGO_HOST=host:port jive3.DbReader SinusGen/1
#---------------------------------------------------------
# CLASS SinusGen properties
#---------------------------------------------------------

CLASS/SinusGen->Description: "A Class to generate sinus curve."
CLASS/SinusGen->InheritedFrom: TANGO_BASE_CLASS
CLASS/SinusGen->ProjectTitle: "Sinus wave generator"

#---------------------------------------------------------
# SERVER SinusGen/1, SinusGen device declaration
#---------------------------------------------------------

SinusGen/1/DEVICE/SinusGen: "jlp/test/1"


# --- jlp/test/1 properties

jlp/test/1->polled_attr: state,\
                         3000
jlp/test/1->toto: 1

# --- jlp/test/1 attribute properties

jlp/test/1/Att_six->display_unit: 1.0
jlp/test/1/Att_six->min_value: 11
jlp/test/1/Att_six->standard_unit: 1.0
jlp/test/1/att_trois->display_unit: 1.0
jlp/test/1/att_trois->max_value: 100.0
jlp/test/1/att_trois->min_value: -10.0
jlp/test/1/att_trois->standard_unit: 1.0


# --- dserver/SinusGen/1 properties

dserver/SinusGen/1->polling_threads_pool_conf: "jlp/test/1"
```

_Load a property file into the Database_
```console
>java -DTANGO_HOST=host:port jive3.DbWriter test.res
```
