package jive3;

import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import jive.JiveUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.IOException;

/**
 * Display device usage
 */
public class UsageDlg extends JDialog implements ActionListener {

  private JTextArea   resText;
  private JScrollPane resScrollPane;
  private JButton     dismissButton;
  private boolean     okFlag;

  public UsageDlg(JFrame parent,String devName) {

    super(parent,false);

    JPanel innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    resText = new JTextArea();
    resText.setFont(new Font("Monospaced",Font.PLAIN,12));
    resScrollPane = new JScrollPane(resText);
    resScrollPane.setPreferredSize(new Dimension(600,400));
    innerPanel.add(resScrollPane,BorderLayout.CENTER);

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

    dismissButton = new JButton("Dismiss");
    dismissButton.addActionListener(this);
    buttonPanel.add(dismissButton);

    innerPanel.add(buttonPanel,BorderLayout.SOUTH);

    setTitle("Device Usage [" + devName + "]");
    setContentPane(innerPanel);

  }

  public void setText(String text) {
    resText.setText(text);
  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();
    if ( src== dismissButton) {
      setVisible(false);
    }

  }

  public void showDlg() {

    ATKGraphicsUtils.centerDialog(this);
    setVisible(true);

  }

}
