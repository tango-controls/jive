package jive3;

import jive.MultiLineCellEditor;
import jive.MultiLineCellRenderer;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

import static java.awt.event.KeyEvent.*;

/* A specific table for Jive */
public class JiveTable extends JTable {

  class ModifiedKeyListener {
    KeyListener listener;
    int keyCode;
    int keyModifier;
    ModifiedKeyListener(KeyListener listener, int keyCode, int keyModifier) {
      this.listener = listener;
      this.keyCode = keyCode;
      this.keyModifier = keyModifier;
    }
  }

  private MultiLineCellEditor editor;
  private MainPanel parent = null;
  private Vector<ModifiedKeyListener> modifiedKeyListenerList = new Vector<ModifiedKeyListener>();

  public JiveTable(TableModel model) {

    super(model);
    editor = new MultiLineCellEditor(this);
    setDefaultEditor(String.class, editor);

    MultiLineCellRenderer renderer = new MultiLineCellRenderer(false,true,true);
    setDefaultRenderer(String.class, renderer);

  }

  public void setParent(MainPanel parent) {
    this.parent = parent;
  }

  protected void processEvent(AWTEvent e) {

    if( e instanceof MouseEvent ) {
      MouseEvent me = (MouseEvent)e;
      if(me.getButton()==1 && me.getID()==MouseEvent.MOUSE_PRESSED) {
        int column = getColumnForLocation(me.getX());
        if( column==1 ) {
          int row = getRowForLocation(me.getY());
          String value = (String)getModel().getValueAt(row,column);
          MultiLineCellRenderer c = (MultiLineCellRenderer)getCellRenderer(row, column);
          c.setText(value);
          if( c.hasDevice() ) {
            Rectangle rect = getCellRect(row,column,false);
            int x = me.getX() - rect.x;
            int y = me.getY() - rect.y;
            String name = c.getDevice(x,y);
            if( name!=null && parent!=null ) {
              // Go to device node
              parent.goToDeviceNode(name);
              return;
            }
          }
        }
      }
    } else if ( e instanceof KeyEvent ) {

      // Key shortcut in non edit mode
      if (!isEditing()) {
        boolean found = false;
        KeyEvent ke = (KeyEvent) e;
        int i = 0;
        while (!found && i < modifiedKeyListenerList.size()) {
          ModifiedKeyListener kl = modifiedKeyListenerList.get(i);
          found = (kl.keyCode == ke.getKeyCode()) && (ke.getModifiersEx() == kl.keyModifier);
          if (!found) i++;
        }
        if (found) {
          switch (e.getID()) {
            case KEY_PRESSED:
              modifiedKeyListenerList.get(i).listener.keyPressed(ke);
              return;
            case KEY_RELEASED:
              modifiedKeyListenerList.get(i).listener.keyReleased(ke);
              return;
            case KEY_TYPED:
              modifiedKeyListenerList.get(i).listener.keyTyped(ke);
              return;
          }
        }
      }

    }
    super.processEvent(e);
  }

  public MultiLineCellEditor getEditor() {
    return editor;
  }

  public void updateRows() {
    editor.updateRows();
  }

  public int getRowForLocation(int y) {

    boolean found = false;
    int i = 0;
    int h = 0;

    while(i<getModel().getRowCount() && !found) {
      found = (y>=h && y<=h+getRowHeight(i));
      if(!found) {
        h+=getRowHeight(i);
        i++;
      }
    }

    if(found) {
      return i;
    } else {
      return -1;
    }

  }

  public int getColumnForLocation(int x) {

    boolean found = false;
    int i = 0;
    int w = 0;

    while(i<getModel().getColumnCount() && !found) {
      int cWidth = getColumnModel().getColumn(i).getWidth();
      found = (x>=w && x<=w+cWidth);
      if(!found) {
        w+=cWidth;
        i++;
      }
    }

    if(found) {
      return i;
    } else {
      return -1;
    }

  }

  public void addKeyPressedEvent(int keyCode,int modifiers,KeyListener l) {
    modifiedKeyListenerList.add( new ModifiedKeyListener(l,keyCode,modifiers) );
  }

}
