package jive3;

import fr.esrf.tangoatk.widget.util.ATKConstant;
import jive.JiveUtils;
import jive.MultiLineCellEditor;
import jive.MultiLineCellRenderer;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

/**
 * Coherency checking between file and database
 */
public class DiffDlg extends JFrame implements ActionListener {

  private final static String colNames[] = {"Property", "Database value", "File value"};

  private JScrollPane textView;
  private JTable[]    tables;

  private JButton     dismissBtn;
  private JPanel      btnPanel;
  private JPanel      innerPanel;

  public DiffDlg(Vector<Vector<String>> diffs, File[] fileNames) {

    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    JPanel tablesPanel = new JPanel();
    tablesPanel.setLayout(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.BOTH;
    gbc.gridx = 0;
    gbc.weightx = 1.0;
    tables = new JTable[fileNames.length];
    int gy = 0;

    for(int i=0;i<fileNames.length;i++) {

      // Table initialisation
      DefaultTableModel dm = new DefaultTableModel() {
        public Class getColumnClass(int columnIndex) {
          return String.class;
        }
        public boolean isCellEditable(int row, int column) { return false; }
        public void setValueAt(Object aValue, int row, int column) { }
      };
      tables[i] = new JTable(dm);
      JLabel fLabel = new JLabel(fileNames[i].getAbsolutePath());
      MultiLineCellEditor editor = new MultiLineCellEditor(tables[i]);
      tables[i].setDefaultEditor(String.class, editor);
      MultiLineCellRenderer renderer = new MultiLineCellRenderer();
      tables[i].setDefaultRenderer(String.class, renderer);

      // Fill table
      Vector<String> diff = diffs.get(i);

      if( diff.size()==1 ) {

        gbc.gridy = gy++;
        gbc.insets.top = 10;
        gbc.insets.bottom = 2;
        tablesPanel.add(fLabel, gbc);
        JLabel label = new JLabel(diff.get(0));
        label.setFont(ATKConstant.labelFont);
        gbc.insets.top = 0;
        gbc.insets.bottom = 0;
        gbc.gridy = gy++;
        tablesPanel.add(label, gbc);

      } else {

        String[][] prop = new String[diff.size() / 3][3];
        for (int j = 0; j < diff.size(); j += 3) {
          prop[j / 3][0] = diff.get(j);
          prop[j / 3][1] = diff.get(j + 1);
          prop[j / 3][2] = diff.get(j + 2);
        }

        // Add to panel
        dm.setDataVector(prop, colNames);
        editor.updateRows();
        tables[i].validate();

        gbc.gridy = gy++;
        gbc.insets.top = 10;
        gbc.insets.bottom = 2;
        tablesPanel.add(fLabel, gbc);
        gbc.insets.top = 0;
        gbc.insets.bottom = 0;
        gbc.gridy = gy++;
        tablesPanel.add(tables[i].getTableHeader(), gbc);
        gbc.gridy = gy++;
        tablesPanel.add(tables[i], gbc);

      }

    }

    // Align to top
    gbc.gridy = gy;
    gbc.weighty = 1.0;
    tablesPanel.add(new JPanel(),gbc);

    textView = new JScrollPane(tablesPanel);
    textView.setPreferredSize(new Dimension(800,600));
    add(textView, BorderLayout.CENTER);
    innerPanel.add(textView,BorderLayout.CENTER);


    // Button panel
    btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    innerPanel.add(btnPanel,BorderLayout.SOUTH);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    btnPanel.add(dismissBtn);

    setContentPane(innerPanel);
    setTitle("DB diff");

  }


  public void actionPerformed(ActionEvent e) {

    if( e.getSource()==dismissBtn ) {
      setVisible(false);
    }

  }

}
